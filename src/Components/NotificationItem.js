// @flow
import React from 'react';
import {View, Text, TouchableOpacity, Image, StyleSheet} from 'react-native';

import business from '../../assets/business.png';
import web from '../../assets/web.png';
import mobile from '../../assets/mobile.png';
import tech from '../../assets/tech.png';
import others from '../../assets/others.png';
import startup from '../../assets/startup.png';

const getBorderColor = (category) => {
  switch (category) {
    case 'web':
      return '#f29753';
    case 'tech':
      return '#3e2f96';
    case 'mobile':
      return '#cf4451';
    case 'business':
      return '#1a73e8';
    case 'startup':
      return '#f1c40f';
    default:
      return '#1abb9c';
  }
};

const getBackgroundColor = (category) => {
  switch (category) {
    case 'web':
      return '#f2975350';
    case 'tech':
      return '#3e2f9650';
    case 'mobile':
      return '#cf445150';
    case 'business':
      return '#1a73e850';
    case 'startup':
      return '#f1c40f50';
    default:
      return '#1abb9c50';
  }
};

const getStyles = (category) =>
  StyleSheet.create({
    wrapper: {
      flexDirection: 'row',
      flex: 1,
      padding: 8,
      borderColor: getBorderColor(category),
      borderWidth: 2,
      margin: 8,
      borderRadius: 10,
      backgroundColor: getBackgroundColor(category),
    },
    image: {
      width: 70,
      height: 70,
    },
    textWrapper: {
      justifyContent: 'center',
    },
    heading: {
      fontSize: 18,
      paddingBottom: 8,
      fontWeight: '500',
      color: '#333333',
      letterSpacing: 1,
    },
    description: {
      fontSize: 14,
      color: '#000000',
    },
  });

type Props = {
  title: string,
  description: string,
  category: string,
};

const NotificationItem = (props: Props) => {
  const {title, description, category} = props;
  const styles = getStyles(category);

  const getIcon = () => {
    switch (category) {
      case 'tech':
        return <Image source={tech} style={styles.image} />;
      case 'business':
        return <Image source={business} style={styles.image} />;
      case 'web':
        return <Image source={web} style={styles.image} />;
      case 'mobile':
        return <Image source={mobile} style={styles.image} />;
      case 'startup':
        return <Image source={startup} style={styles.image} />;
      default:
        return <Image source={others} style={styles.image} />;
    }
  };

  return (
    <TouchableOpacity style={styles.wrapper}>
      <View>{getIcon()}</View>
      <View style={styles.textWrapper}>
        <Text style={styles.heading}>{title}</Text>
        <Text style={styles.description}>{description}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default NotificationItem;

// @flow
import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';

type Props = {
  news: *,
  navigation: *,
  startingIndex: number,
  lastIndex: number,
};

const FeaturedNews = (props: Props) => {
  const {
    news: {
      description,
      source: {name},
      title,
      url,
      urlToImage,
      author,
    },
    navigation,
  } = props;
  return (
    <TouchableOpacity
      style={styles.wrapper}
      onPress={() =>
        navigation.navigate('ViewNews', {
          url: url,
        })
      }>
      <>
        <View style={styles.imageWrapper}>
          <Image source={{uri: urlToImage}} style={styles.image} />
        </View>
        <View style={styles.contentWrapper}>
          <View style={styles.headingWrapper}>
            <Text style={styles.title}>{title}</Text>
          </View>
          <View style={styles.descriptionWrapper}>
            <Text style={styles.description}>{description}</Text>
            <Text style={styles.swipe}>Swipe left for more {name} news</Text>
          </View>
        </View>
        <View style={styles.authorInfo}>
          <Text>by {author}</Text>
        </View>
      </>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  authorInfo: {
    flex: 1,
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  wrapper: {
    backgroundColor: '#ffffff',
    height: '100%',
  },
  title: {
    fontSize: 20,
    paddingHorizontal: 8,
    fontWeight: '500',
    color: '#333333',
    textAlign: 'justify',
  },
  imageWrapper: {
    flex: 4,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  contentWrapper: {
    flex: 5,
  },
  headingWrapper: {
    flexDirection: 'column',
    paddingHorizontal: 8,
    paddingTop: 16,
  },
  descriptionWrapper: {
    paddingTop: 8,
    paddingHorizontal: 16,
  },
  description: {
    fontSize: 16,
    color: '#616161',
    letterSpacing: 0.5,
    lineHeight: 24,
  },
  swipe: {
    paddingTop: 8,
    color: 'grey',
  },
});

export default FeaturedNews;

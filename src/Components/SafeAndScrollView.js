// @flow
import React from 'react';
import {SafeAreaView, ScrollView} from 'react-native';

type Props = {
  children: *,
};

const SafeAndScrollView = (props: Props) => {
  return (
    <SafeAreaView>
      <ScrollView>{props.children}</ScrollView>
    </SafeAreaView>
  );
};

export default SafeAndScrollView;
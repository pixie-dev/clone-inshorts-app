const data = [
  {
    author: 'Sarah Perez',
    content:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all',
    description:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all. The app industry is as hot as ever, with a record 204 billion downloads and $120 billion in consu…',
    publishedAt: '2020-06-20T19:53:57Z',
    source: {id: 'techcrunch', name: 'TechCrunch'},
    title:
      'This Week in Apps: App Store outrage, WWDC20 prep, Android subscriptions change',
    url:
      'https://techcrunch.com/2020/06/20/this-week-in-apps-app-store-outrage-wwdc20-prep-android-subscriptions-change/',
    urlToImage:
      'https://techcrunch.com/wp-content/uploads/2020/02/this-week-in-apps-splash.png?w=753',
  },
  {
    author: 'Sarah Perez',
    content:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all',
    description:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all. The app industry is as hot as ever, with a record 204 billion downloads and $120 billion in consu…',
    publishedAt: '2020-06-20T19:53:57Z',
    source: {id: 'techcrunch', name: 'TechCrunch'},
    title:
      'This Week in Apps: App Store outrage, WWDC20 prep, Android subscriptions change',
    url:
      'https://techcrunch.com/2020/06/20/this-week-in-apps-app-store-outrage-wwdc20-prep-android-subscriptions-change/',
    urlToImage:
      'https://techcrunch.com/wp-content/uploads/2020/02/this-week-in-apps-splash.png?w=753',
  },
  {
    author: 'Sarah Perez',
    content:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all',
    description:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all. The app industry is as hot as ever, with a record 204 billion downloads and $120 billion in consu…',
    publishedAt: '2020-06-20T19:53:57Z',
    source: {id: 'techcrunch', name: 'TechCrunch'},
    title:
      'This Week in Apps: App Store outrage, WWDC20 prep, Android subscriptions change',
    url:
      'https://techcrunch.com/2020/06/20/this-week-in-apps-app-store-outrage-wwdc20-prep-android-subscriptions-change/',
    urlToImage:
      'https://techcrunch.com/wp-content/uploads/2020/02/this-week-in-apps-splash.png?w=753',
  },
  {
    author: 'Sarah Perez',
    content:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all',
    description:
      'Welcome back to This Week in Apps, the Extra Crunch series that recaps the latest OS news, the applications they support and the money that flows through it all. The app industry is as hot as ever, with a record 204 billion downloads and $120 billion in consu…',
    publishedAt: '2020-06-20T19:53:57Z',
    source: {id: 'techcrunch', name: 'TechCrunch'},
    title:
      'This Week in Apps: App Store outrage, WWDC20 prep, Android subscriptions change',
    url:
      'https://techcrunch.com/2020/06/20/this-week-in-apps-app-store-outrage-wwdc20-prep-android-subscriptions-change/',
    urlToImage:
      'https://techcrunch.com/wp-content/uploads/2020/02/this-week-in-apps-splash.png?w=753',
  },
];

export default data;

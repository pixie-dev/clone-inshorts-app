// @flow
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import SafeAndScrollView from '../Components/SafeAndScrollView';

import NotificationItem from '../Components/NotificationItem';

const styles = StyleSheet.create({
  wrapper: {
    paddingVertical: 16,
    paddingHorizontal: 8,
  },
  heading: {
    fontSize: 24,
    textAlign: 'center',
    paddingBottom: 16,
  },
});

const Notification = () => {
  const data = [
    {
      title: 'Tech',
      description: 'lorem ipsum dollar sit amit',
      category: 'tech',
    },
    {
      title: 'Mobile',
      description: 'lorem ipsum dollar sit amit',
      category: 'mobile',
    },
    {
      title: 'Business',
      description: 'lorem ipsum dollar sit amit',
      category: 'business',
    },
    {
      title: 'Web',
      description: 'lorem ipsum dollar sit amit',
      category: 'web',
    },
    {
      title: 'Startup',
      description: 'lorem ipsum dollar sit amit',
      category: 'startup',
    },
    {
      title: 'Others',
      description: 'lorem ipsum dollar sit amit',
      category: 'others',
    },
  ];
  return (
    <SafeAndScrollView>
      <View style={styles.wrapper}>
        <Text style={styles.heading}>Notifications 🔔 </Text>
        {data.map((item) => (
          <NotificationItem
            key={item.title}
            title={item.title}
            description={item.description}
            category={item.category}
          />
        ))}
        <Text style={styles.heading}>
          🎉 Tada...
        </Text>
      </View>
    </SafeAndScrollView>
  );
};

export default Notification;

// @flow
import React, {useState, useEffect} from 'react';
import {
  View,
  ScrollView,
  Dimensions,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import FeaturedNews from '../Components/FeaturedNews';

// import data from '../data';

const DEVICE_WIDTH = Dimensions.get('window').width;

type Props = {
  navigation: *,
};

const Home = (props: Props) => {
  const [data, setData] = useState();
  useEffect(() => {
    fetch(
      'http://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=1cd3a3b8c7104b3081e6a0b917bad72b',
    )
      .then((response) => response.json())
      .then((value) => setData(value.articles));
  }, []);
  console.log(data);
  return (
    <SafeAreaView>
      <View style={styles.container}>
        <ScrollView horizontal={true} pagingEnabled style={styles.wrapper}>
          {data &&
            data.map((item, {index}) => (
              <View style={styles.newsCard} key={item}>
                <FeaturedNews
                  startingIndex={index}
                  lastIndex={data.length}
                  news={item}
                  navigation={props.navigation}
                />
              </View>
            ))}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  wrapper: {
    flex: 1,
  },
  newsCard: {
    width: DEVICE_WIDTH,
    height: '100%',
    backgroundColor: '#ffffff',
  },
});

export default Home;

import React from 'react';
import {ActivityIndicator} from 'react-native';
import {WebView} from 'react-native-webview';

const ViewNews = ({route}) => {
  const {url} = route.params;
  console.log(url, 'url');
  const showLoading = () => {
    return (
      <ActivityIndicator
        style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0}}
        size="large"
      />
    );
  };
  return (
    <>
      <WebView
        startInLoadingState={true}
        source={{uri: url}}
        renderLoading={() => {
          return showLoading();
        }}
      />
    </>
  );
};

export default ViewNews;
